//
//  SchoolListResponseMock.swift
//  NycSchoolTests
//
//  Created by Babu Lal on 3/16/23.
//

import Foundation
@testable import NycSchool

import Foundation
extension SchoolListResponse {
    static func with(success: Bool = true,
                     message: String = "Successfull",
                     data : [SchoolListResponseData] = [SchoolListResponseData(id: 1, datasetName: "d", agencyName: "as", updateFrequency: "1", datasetDescription: "a", datasetKeywords: "a", datasetCategory: "a", details: Details(lastUpdated: "a", dataLastUpdated: "a", metadataLastUpdated: "a", dateCreated: "a", views: "a", downloads: "a", datasetOwner: "a", dateMadePublic: "a", automation: "a"), scores: [Score(dbn: "", schoolName: "a", test: "1", reading: "1", math: "1", writing: "1")])]
                    ) -> SchoolListResponse {
        return SchoolListResponse(
            success: success,
            message: message,
            data : data
        )
    }
}
