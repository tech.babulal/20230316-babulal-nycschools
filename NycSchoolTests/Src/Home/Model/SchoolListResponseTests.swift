//
//  SchoolListResponseTests.swift
//  NycSchoolTests
//
//  Created by Babu Lal on 3/16/23.
//

@testable import NycSchool

import Foundation
import XCTest

class GetSubCategoryResponseTests: XCTestCase {

    func testSuccessParser() {
        let json = """
        {
            "success": true,
            "message": "Successfully generated NYC High School List",
            "data": [
                {
                    "id": 1,
                    "datasetName": "2017 DOE High School Directory",
                    "agencyName": "New York City Department of Education",
                    "updateFrequency": "As Needed",
                    "datasetDescription": "Directory of high schools",
                    "datasetKeywords": "",
                    "datasetCategory": "Education",
                    "details": {
                        "lastUpdated": "May 10, 2022",
                        "dataLastUpdated": "January 3, 2019",
                        "metadataLastUpdated": "May 10, 2022",
                        "dateCreated": "February 21, 2013",
                        "views": "75.9K",
                        "downloads": "52.2K",
                        "datasetOwner": "NYC OpenData",
                        "dateMadePublic": "2/21/2013",
                        "automation": "No"
                    },
                    "scores": [
                        {
                            "dbn": "01M292",
                            "schoolName": "HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES",
                            "test": "29",
                            "reading": "355",
                            "math": "404",
                            "writing": "363"
                        },
                        {
                            "dbn": "01M448",
                            "schoolName": "UNIVERSITY NEIGHBORHOOD HIGH SCHOOL",
                            "test": "19",
                            "reading": "455",
                            "math": "504",
                            "writing": "463"
                        },
                        {
                            "dbn": "01M450",
                            "schoolName": "EAST SIDE COMMUNITY SCHOOL",
                            "test": "9",
                            "reading": "255",
                            "math": "304",
                            "writing": "263"
                        }
                    ]
                },
                {
                    "id": 2,
                    "datasetName": "2018 DOE High School Directory",
                    "agencyName": "Boston City Department of Education",
                    "updateFrequency": "As Needed",
                    "datasetDescription": "Directory of high schools",
                    "datasetKeywords": "",
                    "datasetCategory": "Education",
                    "details": {
                        "lastUpdated": "May 12, 2022",
                        "dataLastUpdated": "January 4, 2019",
                        "metadataLastUpdated": "May 12, 2022",
                        "dateCreated": "February 23, 2013",
                        "views": "75.9K",
                        "downloads": "52.2K",
                        "datasetOwner": "NYC OpenData",
                        "dateMadePublic": "2/21/2013",
                        "automation": "No"
                    },
                    "scores": [
                        {
                            "dbn": "01M292",
                            "schoolName": "HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES",
                            "test": "29",
                            "reading": "355",
                            "math": "404",
                            "writing": "363"
                        },
                        {
                            "dbn": "01M448",
                            "schoolName": "UNIVERSITY NEIGHBORHOOD HIGH SCHOOL",
                            "test": "19",
                            "reading": "455",
                            "math": "504",
                            "writing": "463"
                        },
                        {
                            "dbn": "01M450",
                            "schoolName": "EAST SIDE COMMUNITY SCHOOL",
                            "test": "9",
                            "reading": "255",
                            "math": "304",
                            "writing": "263"
                        }
                    ]
                },
                {
                    "id": 3,
                    "datasetName": "2019 DOE High School Directory",
                    "agencyName": "California City Department of Education",
                    "updateFrequency": "As Needed",
                    "datasetDescription": "Directory of high schools",
                    "datasetKeywords": "",
                    "datasetCategory": "Education",
                    "details": {
                        "lastUpdated": "May 11, 2022",
                        "dataLastUpdated": "January 4, 2019",
                        "metadataLastUpdated": "May 11, 2022",
                        "dateCreated": "February 22, 2013",
                        "views": "75.9K",
                        "downloads": "52.2K",
                        "datasetOwner": "NYC OpenData",
                        "dateMadePublic": "2/21/2013",
                        "automation": "No"
                    },
                    "scores": [
                        {
                            "dbn": "01M292",
                            "schoolName": "HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES",
                            "test": "29",
                            "reading": "355",
                            "math": "404",
                            "writing": "363"
                        },
                        {
                            "dbn": "01M448",
                            "schoolName": "UNIVERSITY NEIGHBORHOOD HIGH SCHOOL",
                            "test": "19",
                            "reading": "455",
                            "math": "504",
                            "writing": "463"
                        },
                        {
                            "dbn": "01M450",
                            "schoolName": "EAST SIDE COMMUNITY SCHOOL",
                            "test": "9",
                            "reading": "255",
                            "math": "304",
                            "writing": "263"
                        }
                    ]
                }
            ]
        }
        """.data(using: .utf8)!
        
        let movie = try! JSONDecoder().decode(SchoolListResponse.self, from: json)
        
        XCTAssertNotNil(movie)
        XCTAssertEqual(movie.success, SchoolListResponse.with().success)
    }

}
