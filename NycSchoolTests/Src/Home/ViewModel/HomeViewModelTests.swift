//
//  HomeViewModelTests.swift
//  NycSchoolTests
//
//  Created by Babu Lal on 3/16/23.
//

import Foundation
@testable import NycSchool
import XCTest

class HomeViewModelTests: XCTestCase {
    
    func testSuccessGetSchoolListApiCall() {
        let response: SchoolListResponse? = SchoolListResponse.with();
        
        let request: SchoolListRequest? = SchoolListRequest(schoolType: "High");
        
        let service = MockService(success : true, request: request, response: response, error: nil)
        
        let viewModel = HomeViewModel(serviceNetwork: service)
        
        viewModel.getSchoolListApiCall()
        
        XCTAssertTrue(!viewModel.isLoading)
        XCTAssertTrue(viewModel.isSuccess)
        
        
    }
    func testFailureGetSchoolListApiCall() {
        let response: SchoolListResponse? = nil;
        
        let errorTemp = NSError(domain:"", code:400, userInfo:nil)
        
        let request: SchoolListRequest? = SchoolListRequest(schoolType: "High");
        
        let service = MockService(success : true, request: request, response: response, error: errorTemp)
        
        let viewModel = HomeViewModel(serviceNetwork: service)
        
        viewModel.getSchoolListApiCall()
        
        XCTAssertTrue(!viewModel.isLoading)
        XCTAssertFalse(viewModel.isSuccess)
        
        
    }
    
}
