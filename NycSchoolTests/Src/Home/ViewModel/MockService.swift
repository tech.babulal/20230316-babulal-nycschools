//
//  MockService.swift
//  NycSchoolTests
//
//  Created by Babu Lal on 3/16/23.
//

@testable import NycSchool
import Foundation
import Combine

class MockService : NetworkManagerProtocol {
    
    let success : Bool
    let request: SchoolListRequest?
    let response: SchoolListResponse?
    let error : Error?
    
    init(success : Bool,request: SchoolListRequest?, response: SchoolListResponse?, error : Error?) {
        self.success = success
        self.request = request
        self.response = response
        self.error = error
    }
    
    func fetchSchoolList(request: SchoolListRequest) -> Future<SchoolListResponse, Error> {
        return Future<NycSchool.SchoolListResponse, Error> { promise in
            
            if(self.response != nil){
                promise(.success(self.response!))
            }else {
                promise(.failure(self.error!))
            }
        }
    }
}
