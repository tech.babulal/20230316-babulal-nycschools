//
//  ButtonStyle.swift
//  20230316-BabuLal-NYCSchools
//
//  Created by Babu Lal on 3/16/23.
//

import Foundation
import SwiftUI
struct RetryButtonStyle: ButtonStyle {
    
    func makeBody(configuration: ButtonStyle.Configuration) -> some View {
        MyButton( configuration: configuration )
    }
    
    struct MyButton: View {
        let configuration: ButtonStyle.Configuration
        @Environment(\.isEnabled) private var isEnabled: Bool
        var body: some View {
            configuration.label
                .padding(EdgeInsets(top: CGFloat.theme.mediumSpacing, leading: CGFloat.theme.extraLargeSpacing,bottom:CGFloat.theme.mediumSpacing, trailing: CGFloat.theme.extraLargeSpacing))
                .background(isEnabled ? Color.theme.error : Color.theme.button.opacity(0.2))
                .foregroundColor(isEnabled ? Color.theme.buttonText : Color.theme.buttonText.opacity(0.3))
                .cornerRadius(CGFloat.theme.cornerRadius)
                .font(.caption)
                .scaleEffect(configuration.isPressed ? 1.2 : 1)
                .animation(.easeOut(duration: 0.2), value: configuration.isPressed
                )
        }
    }
}
