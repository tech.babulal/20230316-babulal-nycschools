//
//  Logger.swift
//  NycSchool
//
//  Created by Babu Lal on 3/28/23.
//

import Foundation
//protocol Logging {
//    func log(_ message: String)
//}
class Logger {
    static let shared = Logger()

    private init() { }

    func log(_ title: String, _ message: String) {
        print("\(title) :: \(message)")
    }
}

//extension Logging {
//    func log(_ message: String) {
//        Logger.shared.log(message)
//    }
//}
