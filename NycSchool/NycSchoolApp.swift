//
//  NycSchoolApp.swift
//  NycSchool
//
//  Created by Babu Lal on 3/16/23.
//

import SwiftUI

@main
struct NycSchoolApp: App {
    
    var body: some Scene {
        WindowGroup {
            CoordinatorView()
        }
    }
}
