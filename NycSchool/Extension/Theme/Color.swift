//
//  Color.swift
//  20230316-BabuLal-NYCSchools
//
//  Created by Babu Lal on 3/16/23.
//

import Foundation
import SwiftUI

extension Color {
    static let theme = ColorTheme();
}

struct ColorTheme{
    let accent = Color("AccentColor")
    let background = Color("BackgroundColor")
    let green = Color("GreenColor")
    let primaryText = Color("PrimaryTextColor")
    let red = Color("RedColor")
    let secondaryText = Color("SecondaryTextColor")
    let border = Color("BorderColor")
    let button = Color("ButtonColor")
    let foreground = Color("ForegroundColor")
    let brandPrimary = Color("BrandColorPrimary")
    let brandSecondary = Color("BrandColorSecondary")
    let menuBackground = Color("MenuBackgroundColor")
    let menuItemBackground = Color("MenuItemBackgroundColor")
    let menuItemText = Color("MenuItemTextColor")
    let buttonText = Color("ButtonTextColor")
    let icon = Color("IconColor")
    let progressBar = Color("ProgressBarColor")
    let error = Color("Error")
    let primary = Color("Primary")
    let secondary = Color("Secondary")
    let toolbarBackground = Color("ToolBarBackground")
    let heartRate = Color("HeartRate")
    let steps = Color("Steps")
    let lightGrey = Color("LightGrey")
}
