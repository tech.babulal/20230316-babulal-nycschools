//
//  CGFloat.swift
//  20230316-BabuLal-NYCSchools
//
//  Created by Babu Lal on 3/16/23.
//

import Foundation

extension CGFloat {
    static let theme = CGFloatTheme();
}

struct CGFloatTheme{
    let extraLargeSpacing:CGFloat = 32.0
    let largeSpacing:CGFloat  = 16.0
    let  mediumSpacing:CGFloat = 12.0
    let smallSpacing:CGFloat = 8.0
    let xsSpacing:CGFloat = 4.0
    let xxsSpacing:CGFloat = 2.0
    let xxxsSpacing:CGFloat = 1.0
    let cornerRadius:CGFloat = 16.0
    let lineWidth:CGFloat = 0.7
    let logoHeight:CGFloat = 80.0
    let logoWidth:CGFloat = 80.0
    let menuWidth:CGFloat = 270.0
    let menuItemOpacity:CGFloat = 0.4
    let textFieldSpacing:CGFloat = 10.0
}
