//
//  TestView.swift
//  NycSchool
//
//  Created by Babu Lal on 3/28/23.
//

import SwiftUI

struct TestView: View {
    
    let data : String
    
    init(data: String) {
        self.data = data
    }
    var body: some View {
        Text("Hello, World! : \(data)")
    }
}

