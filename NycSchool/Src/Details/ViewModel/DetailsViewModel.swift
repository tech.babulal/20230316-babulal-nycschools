//
//  DetailsViewModel.swift
//  NycSchool
//
//  Created by Babu Lal on 3/16/23.
//

import Foundation
import Combine

class DetailsViewModel: ObservableObject {
  
    var data: SchoolListResponseData
    
    init(data: SchoolListResponseData) {
        print("DetailsViewModel init")
        self.data = data
    }
    
}
