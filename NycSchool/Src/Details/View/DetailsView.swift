//
//  DetailsView.swift
//  NycSchool
//
//  Created by Babu Lal on 3/16/23.
//

import SwiftUI

struct DetailsView: View {
    
    @ObservedObject  var viewModel : DetailsViewModel
    
    var body: some View {
        
        
        VStack(alignment: .center) {
            if(self.viewModel.data.scores != nil){
                Text(viewModel.data.datasetName ?? "").font(.headline).foregroundColor(Color.theme.brandPrimary)
                Text(viewModel.data.agencyName ?? "").font(.subheadline).foregroundColor(Color.theme.primaryText)
                Text(viewModel.data.datasetDescription ?? "").font(.caption).foregroundColor(Color.theme.secondaryText)
                Text(LocalizedStringKey("scoreDetails")).font(.headline).foregroundColor(Color.theme.brandSecondary).padding(.top,CGFloat.theme.mediumSpacing)
                List(viewModel.data.scores!, id: \.dbn) { score in
                    CustomScoreView(data: score)
                }.padding(.top, CGFloat.theme.xxxsSpacing).listStyle(.plain)
            } else {
                VStack{
                    Text(LocalizedStringKey("noMoreDetailsAvailable"))
                }
            }
        }.padding(CGFloat.theme.smallSpacing)
            .navigationTitle(Text(LocalizedStringKey("details")))
            .navigationBarTitleDisplayMode(.inline)
        
    }
}
