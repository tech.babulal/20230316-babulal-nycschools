//
//  CustomScoreView.swift
//  NycSchool
//
//  Created by Babu Lal on 3/16/23.
//

import SwiftUI

struct CustomScoreView: View {
    
    var data: Score
    
    init(data: Score) {
        self.data = data
    }
    
    var body: some View {
        VStack(alignment: .leading){
            Text(data.schoolName ?? "").font(.subheadline).foregroundColor(Color.theme.brandPrimary)
            
            CustomScoreListView(hint: "dbn", value: data.dbn)
            CustomScoreListView(hint: "test", value: data.test)
            CustomScoreListView(hint: "reading", value: data.reading)
            CustomScoreListView(hint: "math", value: data.math)
            CustomScoreListView(hint: "writing", value: data.writing)
        }.padding(.top,CGFloat.theme.xxxsSpacing)
    }
}

/*
 MARK: - CustomScoreListView
 This View for showing hint and value
 */
struct CustomScoreListView: View {
    
    var hint : String?
    var value : String?
    
    init(hint: String?, value : String?){
        self.hint = hint
        self.value = value
    }
    
    var body: some View {
        HStack(){
            Text(LocalizedStringKey(hint ?? "")).font(.footnote).foregroundColor(Color.theme.secondaryText)
            Spacer()
            Text(value ?? "").font(.footnote).foregroundColor(Color.theme.primaryText)
            
        }
    }
    
    
}
