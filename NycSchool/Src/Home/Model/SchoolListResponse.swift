//
//  SchoolListResponse.swift
//  20230316-BabuLal-NYCSchools
//
//  Created by Babu Lal on 3/16/23.
//

import Foundation

// MARK: - SchoolListResponse
struct SchoolListResponse: Codable {
    let success: Bool?
    let message: String?
    let data: [SchoolListResponseData]?
}

// MARK: - SchoolListResponseData
struct SchoolListResponseData:  Codable, Hashable {
  
    let id: Int?
    let datasetName, agencyName, updateFrequency, datasetDescription: String?
    let datasetKeywords, datasetCategory: String?
    let details: Details?
    let scores: [Score]?
    
    static func ==(lhs: SchoolListResponseData, rhs: SchoolListResponseData) -> Bool {
        return lhs.details?.hashValue == rhs.details?.hashValue &&  lhs.scores?.hashValue ==  rhs.details?.hashValue
    }
}



// MARK: - Details
struct Details: Codable, Hashable {
    let lastUpdated, dataLastUpdated, metadataLastUpdated, dateCreated: String?
    let views, downloads, datasetOwner, dateMadePublic: String?
    let automation: String?
}

// MARK: - Score
struct Score: Codable,Hashable {
    let dbn, schoolName, test, reading: String?
    let math, writing: String?
}








