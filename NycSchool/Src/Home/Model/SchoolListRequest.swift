//
//  SchoolListRequest.swift
//  20230316-BabuLal-NYCSchools
//
//  Created by Babu Lal on 3/16/23.
//

import Foundation

// MARK: - SchoolListRequest
struct SchoolListRequest: Codable {
    let schoolType: String
}
