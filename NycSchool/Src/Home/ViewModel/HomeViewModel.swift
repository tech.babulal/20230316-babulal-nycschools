//
//  HomeViewModel.swift
//  20230316-BabuLal-NYCSchools
//
//  Created by Babu Lal on 3/16/23.
//

import Foundation
import Combine

class HomeViewModel: ObservableObject {
    
    @Published var isLoading = false
    @Published var isSuccess = false
    @Published  var errorMessage : String = ""
    let serviceNetwork : NetworkManagerProtocol
    private var cancellables = [AnyCancellable]()
    @Published var response: SchoolListResponse?
    
    init( serviceNetwork : NetworkManagerProtocol = NetworkService()) {
        self.serviceNetwork = serviceNetwork
        getSchoolListApiCall()
    }
    
    /*
     MARK: - GetSchoolListApiCall
     This api call get list of all schools list according to request parameter.
     */
    func getSchoolListApiCall(){
        
        
        
        self.isLoading = true
        serviceNetwork.fetchSchoolList(request: SchoolListRequest(schoolType: "High")).sink { completion in
            
            switch completion {
            case .failure(let err):
                self.isLoading = false
                self.isSuccess = false
                self.errorMessage = err.localizedDescription
               

            case .finished:
                self.isLoading = false
                self.isSuccess = true
                self.errorMessage = ""
              
            } } receiveValue: { data in
                
                self.response = data
                self.isLoading = false
                self.isSuccess = true
                self.errorMessage = ""
              
            }.store(in: &cancellables)
        
    }
    
    
    
}
