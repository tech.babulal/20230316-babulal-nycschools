//
//  HomeView.swift
//  20230316-BabuLal-NYCSchools
//
//  Created by Babu Lal on 3/16/23.
//

import SwiftUI

struct HomeView: View {
    
    @EnvironmentObject var coordinator: Coordinator<Path>

    @StateObject  var viewModel : HomeViewModel

    var body: some View {
        
        VStack(alignment: .leading) {
            
            if  self.viewModel.isLoading == false  {
                if(self.viewModel.response?.data != nil && self.viewModel.isSuccess == true){
                    List(viewModel.response!.data!, id: \.id) { school in
                        
                        CustomSchoolListView(data: school)
                            .onTapGesture {
                               
                                Logger.shared.log("HomeView", "Click")
                                coordinator.push(.details(data: school))
                            }
                    }
                } else {
                    VStack{
                        Text(viewModel.errorMessage)
                        Button(LocalizedStringKey("retry")){
                            self.viewModel.getSchoolListApiCall()
                        }.buttonStyle(RetryButtonStyle())
                    }
                }
            } else {
                ProgressView(LocalizedStringKey("loading"))
            }
            
          
        }.navigationTitle(Text(LocalizedStringKey("schoolList")))
            .navigationBarTitleDisplayMode(.inline).onAppear(){
                
                Logger.shared.log("HomeView", "onAppear")
            }
        
        
    }
}

