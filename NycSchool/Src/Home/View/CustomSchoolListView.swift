//
//  CustomSchoolListView.swift
//  NycSchool
//
//  Created by Babu Lal on 3/16/23.
//

import SwiftUI

struct CustomSchoolListView: View {
    
    var data: SchoolListResponseData
    
    init(data: SchoolListResponseData) {
        self.data = data
    }
    
    var body: some View {
        VStack(alignment: .leading){
            Text(data.datasetName ?? "").font(.headline).foregroundColor(Color.theme.brandPrimary)
            Text(data.agencyName ?? "").font(.subheadline).foregroundColor(Color.theme.primaryText)
            Text(data.datasetDescription ?? "").font(.caption).foregroundColor(Color.theme.secondaryText)
            HStack{
                CustomListViewDetails(hint: "frequency", value: data.updateFrequency)
                Spacer()
                CustomListViewDetails(hint: "category", value: data.datasetCategory)
                Spacer()
                CustomListViewDetails(hint: "views", value: data.details?.views)
                Spacer()
                CustomListViewDetails(hint: "downloads", value: data.details?.downloads)
            }.padding(.top,CGFloat.theme.xxxsSpacing)
        }
    }
}


/*
 MARK: - CustomListViewDetails
 This View for showing hint and value
 */
struct CustomListViewDetails: View {
    
    var hint : String?
    var value : String?
    
    init(hint: String?, value : String?){
        self.hint = hint
        self.value = value
    }
    
    var body: some View {
        VStack(alignment: .leading){
            Text(LocalizedStringKey(hint ?? "")).font(.footnote).foregroundColor(Color.theme.primaryText)
            Text(value ?? "").font(.footnote).foregroundColor(Color.theme.secondaryText)
        }
    }
}

