//
//  RootView.swift
//  NycSchool
//
//  Created by Babu Lal on 3/28/23.
//

import SwiftUI




struct CoordinatorView: View {
    @ObservedObject var router = Coordinator<Path>(root: .home)
    
    var body: some View {
        
        CoordinatorSubView(router: router) { path in
            switch path {
            case .home : HomeView(viewModel: HomeViewModel())
            case .test(let data) : TestView(data: data)
            case .details(let data) : DetailsView(viewModel: DetailsViewModel(data: data))
           
            }
        }.environmentObject(router)
    }
    
}


