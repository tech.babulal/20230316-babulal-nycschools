//
//  Router.swift
//  NycSchool
//
//  Created by Babu Lal on 3/28/23.
//

import Foundation
import SwiftUI

struct CoordinatorSubView<T: Hashable, Content: View>: View {
    
    @ObservedObject
    var router: Coordinator<T>
    
    @ViewBuilder var buildView: (T) -> Content
    var body: some View {
        NavigationStack(path: $router.paths) {
            buildView(router.root)
                .navigationDestination(for: T.self) { path in
                    buildView(path)
                }
        }
        .environmentObject(router)
    }
}

final class Coordinator<T: Hashable>: ObservableObject {
    @Published var root: T
    @Published var paths: [T] = []
    
    init(root: T) {
        self.root = root
    }
    
    func push(_ path: T) {
        paths.append(path)
    }
    
    func pop() {
        paths.removeLast()
    }
    
    func updateRoot(root: T) {
        self.root = root
    }
    
    func popToRoot(){
        paths = []
    }
    
    func pop(to: T) {
        guard let found = paths.firstIndex(where: { $0 == to }) else {
            return
        }
        
        let numToPop = (found..<paths.endIndex).count - 1
        paths.removeLast(numToPop)
    }
}
enum Path : Hashable {
    case home
    case details(data : SchoolListResponseData)
    case test(data : String)
}



