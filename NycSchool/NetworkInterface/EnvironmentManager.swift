//
//  EnvironmentManager.swift
//  20230316-BabuLal-NYCSchools
//
//  Created by Babu Lal on 3/16/23.
//

import Foundation
import Foundation
enum EnvironmentType  {
    case dev
    case staging
    case production
}

class SelectEnvironment {
    static let environment: EnvironmentType = .production
}

class EnvironmentManager {
    var baseUrl: String {
        switch SelectEnvironment.environment {
           case .dev: return String(format: "%@", Bundle.main.object(forInfoDictionaryKey: "BASE_URL_DEV") as! String)
           case .staging: return String(format: "%@", Bundle.main.object(forInfoDictionaryKey: "BASE_URL_STAG") as! String)
           case .production: return String(format: "%@", Bundle.main.object(forInfoDictionaryKey: "BASE_URL") as! String)
        }
    }
}
