//
//  NetworkManagerProtocol.swift
//  20230316-BabuLal-NYCSchools
//
//  Created by Babu Lal on 3/16/23.
//

import Foundation
import Combine
protocol NetworkManagerProtocol : Any{
    
    func fetchSchoolList(request : SchoolListRequest) -> Future<SchoolListResponse, Error>
    
   
}
