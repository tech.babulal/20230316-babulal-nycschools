//
//  NetworkManager.swift
//  20230316-BabuLal-NYCSchools
//
//  Created by Babu Lal on 3/16/23.
//

import Foundation
import Combine

enum Method: String  {
    case get = "GET"
    case post = "POST"
    
}

class NetworkManager {
    
    static let shared = NetworkManager()
    
    private init() {
        
    }
    
    private var cancellables = Set<AnyCancellable>()
    
    
    func getData<T: Decodable>(url: URL, id: Int? = nil, type: T.Type, method : Method, body : Codable) -> Future<T, Error> {
        return Future<T, Error> { [weak self] promise in
            
            //print("URL \(url)")
            var request = URLRequest(url: url)
            request.httpMethod = method.rawValue
            request.httpBody = try? JSONSerialization.data(withJSONObject: body.dictionary ?? [:], options: [])
            //request.setValue("Content-Type", forHTTPHeaderField: "application/json")
            //request.setValue("Accept", forHTTPHeaderField: "application/json")
            
            //print("Request \(body.dictionary ?? [:])")
            URLSession.shared.dataTaskPublisher(for: request)
                .tryMap { (data, response) -> Data in
                    guard let httpResponse = response as? HTTPURLResponse, 200...299 ~= httpResponse.statusCode else {
                        throw NetworkError.responseError
                    }
                    return data
                }
                .decode(type: T.self, decoder: JSONDecoder())
                .receive(on: RunLoop.main)
                .sink(receiveCompletion: { (completion) in
                    if case let .failure(error) = completion {
                        //print("Error \(error.localizedDescription)")

                        promise(.failure(error))
                    }
                    
                }, receiveValue: {
                    //print("Value \($0)")

                    promise(.success($0))
                    
                })
                .store(in: &self!.cancellables)
        }
    }
}


enum NetworkError: Error {
    case invalidURL
    case responseError
    case transportError
    case serverError
    case noData
    case decodingError
    case encodingError
    case unknown
    
    
    
}

extension NetworkError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .invalidURL:
            return NSLocalizedString("Invalid URL", comment: "Invalid URL")
        case .responseError:
            return NSLocalizedString("Unable to get response from server", comment: "Invalid response")
        case .serverError:
            return NSLocalizedString("Server Error", comment: "Server Error")
        case .noData:
            return NSLocalizedString("No data found", comment: "No data found")
        case .decodingError:
            return NSLocalizedString("Unable to decode response", comment: "Unable to decode response")
        case .encodingError:
            return NSLocalizedString("Unable to encode data", comment: "Unable to encode data")
        case .unknown:
            return NSLocalizedString("No internet connection, Make sure connected to internet", comment: "No internet connection, Make sure connected to internet")
        case .transportError:
            return NSLocalizedString("Transport error", comment: "Transport error")
            
        }
    }
}
