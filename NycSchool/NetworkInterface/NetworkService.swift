//
//  NetworkService.swift
//  20230316-BabuLal-NYCSchools
//
//  Created by Babu Lal on 3/16/23.
//

import Foundation
import Combine
private var cancellables = [AnyCancellable]()

class NetworkService : NetworkManagerProtocol{

    func fetchSchoolList(request: SchoolListRequest) -> Future<SchoolListResponse, Error>  {
        return Future<SchoolListResponse, Error> {  promise in
                        
            NetworkManager.shared.getData(url : SeriveURL(path: .schoolList ).url, type: SchoolListResponse.self, method: .post, body: request )
                .sink { completion in
                    switch completion {
                    case .failure(let err):
                        promise(.failure(err))
                    case .finished:
                        Logger.shared.log("LoginApiCall", "Finished")
                    }
                }
        receiveValue: {  data in
            promise(.success(data))
        }
        .store(in: &cancellables)
            
        }
    }
    
}
