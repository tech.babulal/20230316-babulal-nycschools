//
//  SeriveURL.swift
//  20230316-BabuLal-NYCSchools
//
//  Created by Babu Lal on 3/16/23.
//

import Foundation
struct SeriveURL {
    enum Path {

        case schoolList

        fileprivate var components: String {
            switch self {
            case .schoolList:
                return "nycSchoolList"
            }
        }
    }
    private let path: Path
    
    private var baseURL: String {
        
        String(format: "%@", EnvironmentManager().baseUrl)
        
        
    }
    
    init(path: Path) {
        self.path = path
    }
    
    var url: URL {
        let string = "https://\(baseURL)/\(path.components)"
        guard let url = URL(string: string) else { fatalError("SeriveURL conversion to URL failed unexpectedly") }
        return url
    }
}
